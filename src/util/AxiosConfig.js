import axios from 'axios'
import history from '../routing/History'

const baseUrl = `https://railwaysbackend.cfapps.io/`;

export const AXIOS = axios.create({
    baseURL: `${baseUrl}/api/v1`
});

export const axiosNonApi = axios.create({
    baseURL: baseUrl
});

const injectToken = (request) => {
    const token = localStorage.getItem("jwt");
    console.log(request);
    if (token) {
        request.headers.Authorization = `Bearer ${token}`
    }
    return request
};

const logResponse = (response) => {
    console.log(response);
    return response
};

export const redirectIfNotAuthorized = (err) => {
    if (err.response) {
        console.log(err.response);
        if (err.response.status === 401 || err.response.status === 403) {
            history.push('/login');
        }
        console.log(`Response error: ${err.response.data.message}`);
    }
    return Promise.reject(err);
};

AXIOS.interceptors.request.use(injectToken);
AXIOS.interceptors.response.use(logResponse, redirectIfNotAuthorized);
axiosNonApi.interceptors.request.use(injectToken);
axiosNonApi.interceptors.response.use(null, redirectIfNotAuthorized);
