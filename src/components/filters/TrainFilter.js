import Form from "react-bootstrap/Form";
import Select from "react-select";
import React from "react";
import Button from "react-bootstrap/Button";
import {Navbar} from "react-bootstrap";
import {AXIOS} from "../../util/AxiosConfig";

export default class TrainFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            departureCity: null,
            arrivalCity: null,
            trainNumbers: null,
            allTrainNumbers: [],
            allCities: []
        }
    }

    componentDidMount = () => {
        AXIOS.get('/trains/numbers/all')
            .then(response => {
                this.setState({
                    allTrainNumbers: response.data,
                })
            })
            .catch(error => console.log(error));
        AXIOS.get('/stations/cities/all')
            .then(response => {
                this.setState({
                    allCities: response.data,
                })
            })
            .catch(error => console.log(error));
    }

    onDepartureCityChange = (obj) => {
        this.setState({departureCity: obj.value});
    };

    onArrivalCityChange = (obj) => {
        this.setState({arrivalCity: obj.value});
    };

    onTrainNumbersChange = (arr) => {
        if (arr !== null) {
            arr = arr.map(obj => obj.value)
        }
        this.setState({trainNumbers: arr})
    }

    onSubmit = () => {
        this.props.onSubmit({
            cityFrom: this.state.departureCity,
            cityTo: this.state.arrivalCity,
            trainNumbers: this.state.trainNumbers
        })
    }

    makeOptions = (data) => {
        return data.map(obj => {
            return {value: obj, label: obj}
        })
    }

    render() {
        return (
            <div>
                <Navbar bg="light" className={'justify-content-between'}>
                    <Form style={{width: '200px'}}>
                        <Select
                            onChange={this.onDepartureCityChange}
                            placeholder="Откуда"
                            options={this.makeOptions(this.state.allCities)}/>
                    </Form>
                    <Form style={{width: '200px'}}>
                        <Select
                            onChange={this.onArrivalCityChange}
                            placeholder="Куда"
                            options={this.makeOptions(this.state.allCities)}/>
                    </Form>
                    <Form style={{width: '200px'}}>
                        <Select onChange={this.onTrainNumbersChange}
                                isMulti
                                placeholder="Номера поездов"
                                options={this.makeOptions(this.state.allTrainNumbers)}
                                className="basic-multi-select"
                                classNamePrefix="select"
                        />
                    </Form>
                    <Button onClick={this.onSubmit} variant={"danger"}>
                        Найти
                    </Button>
                </Navbar>
            </div>
        );
    }
}