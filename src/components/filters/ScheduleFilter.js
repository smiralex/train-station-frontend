import Form from "react-bootstrap/Form";
import Select from "react-select";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import React from "react";
import Button from "react-bootstrap/Button";
import {Navbar} from "react-bootstrap";
import {AXIOS} from "../../util/AxiosConfig";

export default class ScheduleFilter extends React.Component {

    DATE_REGEXP = /\d{2}-\d{2}-\d{4}/i;

    constructor(props) {
        super(props);
        this.state = {
            departureCity: null,
            arrivalCity: null,
            departureDate: null,
            arrivalDate: null,
            maxDelay: -1,
            minDelay: 0,
            trainNumbers: null,
            seatCategories: [],
            numSeats: 0,
            allSeatCategories: [],
            allTrainNumbers: [],
            allCities: []
        }
    }

    componentDidMount = () => {
        AXIOS.get('/trains/seats/all')
            .then(response => {
                this.setState({
                    allSeatCategories: response.data,
                })
            })
            .catch(error => console.log(error));
        AXIOS.get('/trains/numbers/all')
            .then(response => {
                this.setState({
                    allTrainNumbers: response.data,
                })
            })
            .catch(error => console.log(error));
        AXIOS.get('/stations/cities/all')
            .then(response => {
                this.setState({
                    allCities: response.data,
                })
            })
            .catch(error => console.log(error));
    }

    onFormChange = (event) => {
        this.setState({[event.target.id]: event.target.value});
    };

    onDepartureCityChange = (obj) => {
        this.setState({departureCity: obj.value});
    };

    onArrivalCityChange = (obj) => {
        this.setState({arrivalCity: obj.value});
    };

    onTrainIdsChange = (arr) => {
        if (arr !== null) {
            arr = arr.map(obj => obj.value)
        }
        this.setState({trainNumbers: arr})
    }

    onSeatCategoriesChange = (arr) => {
        if (arr !== null) {
            arr = arr.map(obj => obj.value)
        }
        this.setState({seatCategories: arr})
    }

    onSubmit = () => {
        if (this.isFilterCorrect()) {
            this.props.onSubmit({
                cityFrom: this.state.departureCity,
                cityTo: this.state.arrivalCity,
                dateFrom: this.state.departureDate,
                dateTo: this.state.arrivalDate,
                maxDelay: this.state.maxDelay,
                minDelay: this.state.minDelay,
                trainNumbers: this.state.trainNumbers,
                seatTypes: this.state.seatCategories,
                numSeats: this.state.numSeats,
            })
        }
    }

    isFilterCorrect = () => {
        if ((this.state.arrivalDate !== null && !this.DATE_REGEXP.test(this.state.arrivalDate)) ||
            (this.state.departureDate !== null && !this.DATE_REGEXP.test(this.state.departureDate))) {
            this.props.onError({response: {data: {message: 'Неправильный формат даты'}}})
            return false
        }
        if (isNaN(this.state.maxDelay) || isNaN(this.state.minDelay) ||
            !Number.isInteger(this.state.numSeats)) {
            this.props.onError({response: {data: {message: 'Неправильный формат числовых данных'}}})
            return false;
        }
        return true
    }

    makeOptions = (data) => {
        return data.map(obj => {
            return {value: obj, label: obj}
        })
    }

    render() {
        return (
            <div>
                <Navbar bg="light" className={'justify-content-between'}>
                    <Form style={{width: '200px'}}>
                        <Select
                            onChange={this.onDepartureCityChange}
                            placeholder="Откуда"
                            options={this.makeOptions(this.state.allCities)}/>
                    </Form>
                    <Form style={{width: '200px'}}>
                        <Select
                            onChange={this.onArrivalCityChange}
                            placeholder="Куда"
                            options={this.makeOptions(this.state.allCities)}/>
                    </Form>
                    <Form style={{width: '200px'}}>
                        <Select onChange={this.onTrainIdsChange}
                                isMulti
                                placeholder="Номера поездов"
                                options={this.makeOptions(this.state.allTrainNumbers)}
                                className="basic-multi-select"
                                classNamePrefix="select"
                        />
                    </Form>
                    <Form style={{width: '200px'}}>
                        <Select onChange={this.onSeatCategoriesChange}
                                isMulti
                                placeholder="Типы сидений"
                                options={this.makeOptions(this.state.allSeatCategories)}
                                className="basic-multi-select"
                                classNamePrefix="select"
                        />
                    </Form>
                </Navbar>
                <Navbar bg="light" className="justify-content-between" onChange={this.onFormChange}>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text>
                                Дата выезда
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            id="departureDate"
                            type="text"
                            placeholder="dd-MM-yyyy"
                        />
                    </InputGroup>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text >
                                Дата приезда
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            id="arrivalDate"
                            type="text"
                            placeholder="dd-MM-yyyy"
                        />
                    </InputGroup>
                </Navbar>
                <Navbar bg="light" className="justify-content-between" onChange={this.onFormChange}>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text >
                                Мин. задержка
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            id="minDelay"
                            placeholder="minutes"
                        />
                    </InputGroup>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text >
                                Макс. задержка
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            id="maxDelay"
                            placeholder="minutes"
                        />
                    </InputGroup>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text >
                                Кол-во мест
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl id="numSeats"/>
                    </InputGroup>
                    <Button onClick={this.onSubmit} variant={"danger"}>
                        Найти
                    </Button>
                </Navbar>
            </div>
        );
    }
}