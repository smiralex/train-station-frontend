import React from 'react';
import {Col, Form, Jumbotron, Navbar} from 'react-bootstrap';
import {AXIOS} from "../util/AxiosConfig";

export default class Profile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            fullName: '',
            name: ''
        }
    }


    componentDidMount = () => {
        return AXIOS.get('/profile')
            .then(response => {
                this.setState({
                    email: response.data.email,
                    fullName: response.data.fullName,
                    name: response.data.name,
                })
            })
            .catch(error => console.log(error));
    }

    render() {
        return (
            <Jumbotron>
                <h1>Ваш профиль</h1>
                <Form.Group as={Col}>
                    <Form.Label>Имя пользователя</Form.Label>
                    <Navbar bg="light">
                        <Navbar.Brand>{this.state.name}</Navbar.Brand>
                    </Navbar>
                </Form.Group>
                <Form.Group as={Col}>
                    <Form.Label>Полное имя</Form.Label>
                    <Navbar bg="light">
                        <Navbar.Brand>{this.state.fullName}</Navbar.Brand>
                    </Navbar>
                </Form.Group>
                <Form.Group as={Col}>
                    <Form.Label>Почта</Form.Label>
                    <Navbar bg="light">
                        <Navbar.Brand>{this.state.email}</Navbar.Brand>
                    </Navbar>
                </Form.Group>
            </Jumbotron>
        );
    }
}
