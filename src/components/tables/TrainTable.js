import React from 'react';
import ReactTable from 'react-table'
import {AXIOS} from "../../util/AxiosConfig";
import {Button} from "react-bootstrap";
import {Link} from "react-router-dom";

const columns = [{
    Header: 'Номер\nпоезда',
    accessor: 'trainNumber'
}, {
    Header: 'Тип\nпоезда',
    accessor: 'trainCategory'
}, {
    Header: 'Город\nотправления',
    accessor: 'departureCity'
}, {
    Header: 'Название\nстанции',
    accessor: 'departureStationName'
},{
    Header: 'Город\nприбытия',
    accessor: 'arrivalCity'
}, {
    Header: 'Название\nстанции',
    accessor: 'arrivalStationName'
},{
    Header: 'Новая\nпоездка',
    accessor: 'newRide'
}]



export default class ScheduleTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fullData: [],
            compactData: [],
            numPages: -1,
            pageSize: 0,
            page: 0,
            totalElements: 0,
            loading: false
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.filter !== prevProps.filter && prevProps.filter != null) {
            this.setState({
                numPages: -1,
                pageSize: 0,
                page: 0
            })
            this.fetchData(this.state);
        }
    }

    fetchData = (state, instance) => {
        this.setState({loading: true})
        AXIOS.post('/trains/filter', {
            pageSize: state.pageSize,
            pageNumber: state.page,
            filter: this.props.filter
        })
            .then(response => {
                this.setState({
                    fullData: response.data.elements,
                    compactData: this.compactData(response.data.elements),
                    numPages: response.data.numPages,
                    pageSize: response.data.pageSize,
                    totalElements: response.data.totalElements,
                })
            })
            .catch(error => this.props.onError(error));
        this.setState({
            loading: false
        })
    }

    compactData = (data) => {
        return data.map(elem => {
            return {
                trainNumber: elem.trainNumber,
                trainCategory: elem.trainCategory,
                departureCity: elem.departureCity,
                departureStationName: elem.departureStationName,
                arrivalCity: elem.arrivalCity,
                arrivalStationName: elem.arrivalStationName,
                newRide: <Button as={Link}
                                 to={{
                                     pathname: '/new-ride/' + elem.id,
                                 }}>
                    Новая поездка
                </Button>
            }
        })
    }

    onPageChange = (pageIndex) => {
        this.setState({
            page: pageIndex
        })
    }

    onPageSizeChange = (pageSize, pageIndex) => {
        this.setState({
            page: pageIndex,
            pageSize: pageSize
        })
    }

    render() {
        const table = this.props.showTable ? <ReactTable
            data={this.state.compactData}
            pages={this.state.numPages}
            pageSize={this.state.pageSize}
            loading={this.state.loading}
            page={this.state.page}
            columns={columns}
            manual
            onFetchData={this.fetchData}
            onPageChange={this.onPageChange}
            onPageSizeChange={this.onPageSizeChange}

        /> : null;
        return (
            <div>
                {table}
            </div>
        )
    }
}