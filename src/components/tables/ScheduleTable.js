import React from 'react';
import ReactTable from 'react-table'
import {AXIOS} from "../../util/AxiosConfig";
import {Link} from "react-router-dom";

const columns = [{
    Header: 'Номер поезда',
    accessor: 'trainNumber'
}, {
    Header: 'Город отправления',
    accessor: 'departureCity'
}, {
    Header: 'Время отправления',
    accessor: 'departureDate'
}, {
    Header: 'Город прибытия',
    accessor: 'arrivalCity'
}, {
    Header: 'Время прибытия',
    accessor: 'arrivalDate'
},{
    Header: 'Подробнее',
    accessor: 'details'
}]

export default class ScheduleTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fullData: [],
            compactData: [],
            numPages: -1,
            pageSize: 0,
            pageNumber: 0,
            totalElements: 0,
            loading: false
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.filter !== prevProps.filter) {
            this.fetchData(this.state);
        }
    }

    fetchData = (state, instance) => {
        this.setState({loading: true})
        AXIOS.post('/train-schedule/filter', {
            pageNumber: state.pageNumber,
            pageSize: state.pageSize,
            filter: this.props.filter
        })
        .then(response => {
            this.setState({
                fullData: response.data.elements,
                compactData: this.compactData(response.data.elements),
                numPages: response.data.numPages,
                pageSize: response.data.pageSize,
                totalElements: response.data.totalElements,
            })
        })
        .catch(error => this.props.onError(error));
        this.setState({
            loading: false
        })
    }

    compactData = (data) => {
        return data.map(elem => {
            return {
                trainNumber: elem.trainNumber,
                departureCity: elem.trainDepStCity,
                departureDate: elem.rideDepDate,
                arrivalCity: elem.trainArrStCity,
                arrivalDate: elem.rideArrDate,
                details:
                    <Link to={{
                        pathname: '/train-route/' + elem.rideId,
                        route: elem.route,
                        }}
                >Показать
                </Link>
            }
        })
    }

    onPageChange = (pageIndex) => {
        this.setState({
            pageIndex: pageIndex
        })
    }

    onPageSizeChange = (pageSize, pageIndex) => {
        this.setState({
            pageIndex: pageIndex,
            pageSize: pageSize
        })
    }


    render() {
        const table = this.props.showTable ? <ReactTable
            data={this.state.compactData}
            pages={this.state.numPages}
            pageSize={this.state.pageSize}
            loading={this.state.loading}
            columns={columns}
            manual
            onFetchData={this.fetchData}
            onPageChange={this.onPageChange}
            onPageSizeChange={this.onPageSizeChange}
        /> : null;
        return (
            <div>
                {table}
            </div>
        )
    }
}