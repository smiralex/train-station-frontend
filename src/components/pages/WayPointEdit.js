import React from 'react';
import Jumbotron from "react-bootstrap/Jumbotron";
import {Button, Col, Container, Form, Navbar} from "react-bootstrap";
import {Redirect, withRouter} from "react-router-dom";
import Row from "react-bootstrap/Row";
import {AXIOS} from "../../util/AxiosConfig";
import ModalWindow from "../secondary/ModalWindow";

class WayPointEdit extends React.Component {

    DATETIME_REGEXP = /\d{2}-\d{2}-\d{4} \d{2}:\d{2}/i;

    constructor(props) {
        super(props);
        this.state = {
            arrivalDate: null,
            stayTime: null,
            delay: null,
            showModal: false,
            modalTitle: '',
            modalMessage: '',
            success: false
        }
    }

    onFormChange = (event) => {
        console.log(event.target.id, event.target.value)
        this.setState({[event.target.id]: event.target.value});
    }

    onSubmit = () => {
        if (this.isDataCorrect()) {
            AXIOS.post("/rides/waypoint/edit", {
                rideId: this.props.match.params.rideId,
                stationId: this.props.location.station.stationId,
                arrivalDate: this.state.arrivalDate,
                stayTime: this.state.stayTime,
                delay: this.state.delay,
            })
                .then(response => this.onSuccess(response))
                .catch(error => this.onErrorClose(error));
        }
    }

    isDataCorrect = () => {
        if (isNaN(this.state.delay) || isNaN(this.state.stayTime)) {
            this.onError('Неправильный формат числовых данных')
            return false;
        }
        if (!this.DATETIME_REGEXP.test(this.state.arrivalDate)) {
            this.onError('Неправильный формат даты')
            return false;
        }
        return true;

    }

    onSuccess = (response) => {
        this.setState({
            success: true,
            showModal: true,
            modalMessage: 'Сейчас вы будете перенаправлены в секцию расписаний',
            modalTitle: 'Успешно!'
        });
    }

    onError = (error) => {
        this.setState({
            showModal: true,
            modalMessage: `Поправьте ошибки: ${error}`,
            modalTitle: 'Oшибка!'
        });
    };

    onErrorClose = (error) => {
        this.setState({
            showModal: true,
            modalMessage: `Поправьте ошибки: ${(error.response && error.response.data.message) || 'Неизвестная ошибка'}`,
            modalTitle: 'Oшибка!'
        });
    };

    onModalClose = () => {
        this.setState({
            showModal: false
        });
    };

    render() {
        if (!this.state.showModal && this.state.success) {
            return <Redirect to={"/train-schedule"}/>
        }
        if (this.props.location.station === undefined) {
            return <Redirect to={"/route-edge" + this.props.match.params.rideId}/>
        }
        return (
            <div>
                <ModalWindow
                    show={this.state.showModal}
                    title={this.state.modalTitle}
                    message={this.state.modalMessage}
                    onModalClose={this.onModalClose}
                />
                <Jumbotron>
                    <Container >
                        <Row>
                            <Col>
                                <Form onChange={this.onFormChange}>
                                    <Form.Group>
                                        <Form.Label>Город </Form.Label>
                                        <Navbar bg="light">
                                            <Navbar.Brand>{this.props.location.station.stationCity}</Navbar.Brand>
                                        </Navbar>
                                    </Form.Group>
                                    <Form.Group >
                                        <Form.Label>Название станции</Form.Label>
                                        <Navbar bg="light">
                                            <Navbar.Brand>{this.props.location.station.stationName}</Navbar.Brand>
                                        </Navbar>
                                    </Form.Group>
                                    <Form.Group controlId={'arrivalDate'}>
                                        <Form.Label> Время прибытия</Form.Label>
                                        <Form.Control placeholder={this.props.location.station.arrivalDate} />
                                    </Form.Group>
                                    <Form.Group controlId={'stayTime'}>
                                        <Form.Label> Стоянка (в минутах)</Form.Label>
                                        <Form.Control placeholder={this.props.location.station.stayTime} />
                                    </Form.Group>
                                    <Form.Group controlId={'delay'}>
                                        <Form.Label> Задержка (в минутах)</Form.Label>
                                        <Form.Control placeholder={this.props.location.station.delay} />
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        <Row>
                            <Button onClick={this.onSubmit}>Отправить</Button>
                        </Row>
                    </Container>
                </Jumbotron>
            </div>
        )
    }
}

export default withRouter(WayPointEdit)

