import React from 'react';
import Jumbotron from "react-bootstrap/Jumbotron";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import {Link, Redirect, withRouter} from "react-router-dom";

class TrainRoute extends React.Component {

    render() {
        let route;
        if ( this.props.location.route === undefined) {
            let state = JSON.parse(localStorage.getItem('route'));
            if (state === undefined || state === null) {
                return <Redirect to={"/train-schedule"}/>
            } else {
                route = state.route;
            }
        } else {
            let state =  JSON.stringify({route: this.props.location.route})
            localStorage.setItem('route', state);
            route = this.props.location.route;
        }

        const tbody = route.map((edge, ind) => {
            return (
                <tr>
                    <td>{ind + 1}</td>
                    <td>{edge.depStation.stationCity}</td>
                    <td>{edge.arrStation.stationCity}</td>
                    <td>
                        <Button as={Link} to={{
                            pathname:'/route-edge/' + this.props.match.params.rideId,
                            edge: edge
                            }}>
                        Показать
                        </Button>
                    </td>
                </tr>
            )
        })
        return (
            <Jumbotron>
                <h1>Маршрут</h1>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Город отправления</th>
                        <th>Город прибытия</th>
                        <th>Подробнее</th>
                    </tr>
                    </thead>
                    <tbody>
                        {tbody}
                    </tbody>
                </Table>
            </Jumbotron>
        )
    }
}

export default withRouter(TrainRoute)
