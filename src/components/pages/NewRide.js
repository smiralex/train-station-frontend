import React from 'react';
import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";
import {Redirect, withRouter} from "react-router-dom";
import {AXIOS} from "../../util/AxiosConfig";
import ModalWindow from "../secondary/ModalWindow";
import {Form, Navbar} from "react-bootstrap";

class NewRide extends React.Component {

    DATETIME_REGEXP = /\d{2}-\d{2}-\d{4} \d{2}:\d{2}/i;

    constructor(props) {
        super(props);
        this.state = {
            route: null,
            curStInd: 0,
            curWayPoint: {
                stationId: 0,
                arrivalDate: '',
                stayTime: 0,
                delay: 0
            },
            newWayPoints: [],
            showModal: false,
            modalTitle: '',
            modalMessage: '',
        }
    }

    componentDidMount() {
        AXIOS.get('/rides/last-of-train/' + this.props.match.params.trainId)
            .then(response => {
            if (response.data !== null && response.data !== "") {
                this.setState({
                    route: response.data.route
                })
            } else {
                AXIOS.get('/trains/route-by-train-id/' + this.props.match.params.trainId)
                    .then(response => {
                        if (response.data !== null && response.data !== "") {
                            this.setState({
                                route: response.data
                            })
                        } else {
                            this.setState({
                                end: true
                            })
                        }
                    })
            }
        })
    }

    onFormChange = (event) => {
        const updWp = this.state.curWayPoint;
        updWp[event.target.id] = event.target.value;
        this.setState({curWayPoint: updWp});
    }

    onSubmit = () => {
        if (this.isDataCorrect()) {
            const curWayPoint = this.state.curWayPoint
            curWayPoint.stationId = this.state.route[this.state.curStInd].stationId;
            this.setState({curWayPoint: curWayPoint})
            this.state.newWayPoints.push(this.state.curWayPoint);
            if (this.state.curStInd === this.state.route.length - 1) {
                AXIOS.post("/rides/create", {
                    trainId: this.props.match.params.trainId,
                    route: this.state.newWayPoints
                })
                    .then(response => this.onSuccess(response))
                    .catch(error => this.onErrorClose(error));
            } else {
                this.setState({
                    curStInd: this.state.curStInd + 1,
                    curWayPoint: {
                        stationId: 0,
                        arrivalDate: '',
                        stayTime: 0,
                        delay: 0
                    }
                })
            }
        }
    }

    isDataCorrect = () => {
        if (isNaN(this.state.curWayPoint.delay) || isNaN(this.state.curWayPoint.stayTime)) {
            this.onError('Неправильный формат числовых данных')
            return false;
        }
        if (!this.DATETIME_REGEXP.test(this.state.curWayPoint.arrivalDate)) {
            this.onError('Неправильный формат даты')
            return false
        }
        return true;

    }

    onSuccess = (response) => {
        this.setState({
            end: true,
            showModal: true,
            modalMessage: 'Сейчас вы будете перенаправлены в секцию поездов',
            modalTitle: 'Успешно!'
        });
    }

    onError = (error) => {
        this.setState({
            showModal: true,
            modalMessage: `Поправьте ошибки: ${error}`,
            modalTitle: 'Oшибка!'
        });
    };

    onErrorClose = (error) => {
        this.setState({
            end: true,
            showModal: true,
            modalMessage: `${(error.response && error.response.data.message) || 'Неизвестная ошибка'}.
                Пройдите процедуру создания поездки заново`,
            modalTitle: 'Oшибка!'
        });
    };

    onModalClose = () => {
        this.setState({
            showModal: false
        });
    };

    render() {
        if (!this.state.showModal && this.state.end) {
            return <Redirect to={"/trains"}/>
        }
        if (this.state.route == null) {
            return null
        }
        const buttonText = this.state.curStInd === this.state.route.length - 1 ?
            "Отправить" : "Cледующая станция";
        let stayTime;
        if (this.state.route[this.state.curStInd].stayTime === undefined) {
            stayTime = "Отсутствует"
        } else {
            stayTime = this.state.route[this.state.curStInd].stayTime
        }
        let delay;
        if (this.state.route[this.state.curStInd].delay === undefined) {
            delay = "Отсутствует"
        } else {
            delay = this.state.route[this.state.curStInd].delay
        }
        return (
            <div>
                <ModalWindow
                    show={this.state.showModal}
                    title={this.state.modalTitle}
                    message={this.state.modalMessage}
                    onModalClose={this.onModalClose}
                />
                <Jumbotron>
                    <h1>
                        {"Новая поездка поезда " + this.state.route[0].stationCity + ' - '
                    + this.state.route[this.state.route.length - 1].stationCity}
                    </h1>
                    <Form onChange={this.onFormChange}>
                        <Form.Group>
                            <Form.Label>Город</Form.Label>
                            <Navbar bg="light">
                                <Navbar.Brand>{this.state.route[this.state.curStInd].stationCity}</Navbar.Brand>
                            </Navbar>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Название станции</Form.Label>
                            <Navbar bg="light">
                                <Navbar.Brand>{this.state.route[this.state.curStInd].stationName}</Navbar.Brand>
                            </Navbar>
                        </Form.Group>
                        <Form.Group controlId={'arrivalDate'}>
                            <Form.Label> Время прибытия</Form.Label>
                            <Form.Text>{`Предыдущая поездка: ${this.state.route[this.state.curStInd].arrivalDate || "Отсутствует"}`}</Form.Text>
                            <Form.Control placeholder="dd-mm-yyyy hh:mm" value={this.state.curWayPoint.arrivalDate}/>
                        </Form.Group>
                        <Form.Group controlId={'stayTime'}>
                            <Form.Label> Стоянка (в минутах)</Form.Label>
                            <Form.Text>{`Предыдущая поездка: ${stayTime}`}</Form.Text>
                            <Form.Control value={this.state.curWayPoint.stayTime}/>
                        </Form.Group>
                        <Form.Group controlId={'delay'}>
                            <Form.Label> Задержка (в минутах)</Form.Label>
                            <Form.Text>{`Предыдущая поездка: ${delay}`}</Form.Text>
                            <Form.Control value={this.state.curWayPoint.delay}/>
                        </Form.Group>
                        <Button onClick={this.onSubmit}>{buttonText}</Button>
                    </Form>
                </Jumbotron>
            </div>
        )
    }
}

export default withRouter(NewRide)
