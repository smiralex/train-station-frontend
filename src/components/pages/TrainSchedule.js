import React from 'react';
import {Jumbotron} from "react-bootstrap";
import ScheduleFilter from "../filters/ScheduleFilter";
import ModalWindow from "../secondary/ModalWindow";
import ScheduleTable from "../tables/ScheduleTable";

export default class TrainSchedule extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showTable: false,
            showModal: false,
            modalTitle: '',
            modalMessage: '',
            filter: null
        }
    }

    onFilterSubmit = (filter) => {
        this.setState({
            showTable: true,
            filter: filter
        })
    }

    onError = (error) => {
        this.setState({
            showModal: true,
            showTable: false,
            modalMessage: `Поправьте ошибки: ${(error.response && error.response.data.message) || 'Неизвестная ошибка'}`,
            modalTitle: 'Oшибка!'
        });
    };

    onModalClose = () => {
        this.setState({
            showModal: false
        });
    };

    render() {
        return (
            <div>
                <ModalWindow
                    show={this.state.showModal}
                    title={this.state.modalTitle}
                    message={this.state.modalMessage}
                    onModalClose={this.onModalClose}
                />
                <Jumbotron>
                    <h1 align={'center'}>Расписание</h1>
                    <ScheduleFilter
                        onSubmit={this.onFilterSubmit}
                        onError={this.onError}
                    />
                    <br/>
                    <ScheduleTable
                        showTable={this.state.showTable}
                        filter={this.state.filter}
                        onError={this.onError}
                    />
                </Jumbotron>
            </div>
        );
    }
}
