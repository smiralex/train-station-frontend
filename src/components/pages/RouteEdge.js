import React from 'react';
import Jumbotron from "react-bootstrap/Jumbotron";
import {Button, Col, Container, Form, Navbar} from "react-bootstrap";
import {Link, Redirect, withRouter} from "react-router-dom";
import Row from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";

class RouteEdge extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        let edge;
        if ( this.props.location.edge === undefined) {
            let state = JSON.parse(localStorage.getItem('edge'));
            if (state === undefined || state === null) {
                return <Redirect to={"/train-schedule"}/>
            } else {
                edge = state.edge;
            }
        } else {
            let state =  JSON.stringify({edge: this.props.location.edge})
            localStorage.setItem('edge', state);
            edge = this.props.location.edge;
        }
        const seats = edge.seatsByCategory;
        const tableSeats = []
        for (let seatType in edge.seatsByCategory) {
            tableSeats.push(
                <tr>
                    <td>{seatType}</td>
                    <td>{seats[seatType].numSeats}</td>
                    <td>{seats[seatType].numPurchasedSeats}</td>
                </tr>
            )
        }
        return (
            <Jumbotron>
                <Container >
                    <Row>
                        <Col>
                            <Form>
                                <h2>Станция отправления</h2>
                                <Form.Group as={Col}>
                                    <Form.Label>Город отправления</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.depStation.stationCity}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Название станции</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.depStation.stationName}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Время прибытия</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.depStation.arrivalDate}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Стоянка (в минутах)</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.depStation.stayTime}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Задержка (в минутах)</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.depStation.delay}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                            </Form>
                            <Button as={Link} to={{
                                pathname:'/waypoint-edit/' + this.props.match.params.rideId,
                                station: edge.depStation
                            }}>Редактировать</Button>
                        </Col>
                        <Col>
                            <Form>
                                <h2>Станция прибытия</h2>
                                <Form.Group as={Col}>
                                    <Form.Label>Город прибытия</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.arrStation.stationCity}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Название станции</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.arrStation.stationName}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Время прибытия</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.arrStation.arrivalDate}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Стоянка (в минутах)</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.arrStation.stayTime}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Label>Задержка (в минутах)</Form.Label>
                                    <Navbar bg="light">
                                        <Navbar.Brand>{edge.arrStation.delay}</Navbar.Brand>
                                    </Navbar>
                                </Form.Group>
                            </Form>
                            <Button as={Link} to={{
                                pathname:'/waypoint-edit/' + this.props.match.params.rideId,
                                station: edge.arrStation
                            }}>Редактировать</Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <Table>
                                <thead>
                                <tr>
                                    <th>Тип мест</th>
                                    <th>Всего мест</th>
                                    <th>Занято</th>
                                </tr>
                                </thead>
                                <tbody>
                                {tableSeats}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </Jumbotron>
        )
    }
}

export default withRouter(RouteEdge)

