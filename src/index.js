import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import {AuthProvider} from "./context/AuthProvider";
import history from "./routing/History";
import {Router} from 'react-router-dom';
import RouteSwitch from "./routing/RouteSwitch";
import TrainStationNavBar from "./components/secondary/TrainStationNavBar";
import 'react-table/react-table.css'
import 'bootstrap/dist/css/bootstrap.min.css';

/**
 * Отправная точка приложения. Самый внешний файл
 */

ReactDOM.render(
    <React.StrictMode>
        <Router history={history}>
            <AuthProvider>
                <TrainStationNavBar/>
                <div style={{width: '75%', margin: '0 auto'}}>
                    <RouteSwitch/>
                </div>
            </AuthProvider>
        </Router>,
    </React.StrictMode>,
    document.getElementById('root')
);

serviceWorker.unregister();