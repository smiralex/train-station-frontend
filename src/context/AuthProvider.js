import React from 'react'

const AuthContext = React.createContext();

/**
 * Глобальный контекст для опрделения авторизирован пользователь или нет
 */

class AuthProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthorized: localStorage.getItem("jwt")
        }
    }

    login = (tokens) => {
        localStorage.setItem('jwt', tokens.token);
        console.log('token: ' + localStorage.getItem("jwt"))
        this.setState({isAuthorized: true});
    };

    logout = () => {
        localStorage.removeItem('jwt');
        this.setState({isAuthorized: false})
    };

    render() {
        return (
            <AuthContext.Provider value={{
                login: this.login,
                logout: this.logout,
                isAuthorized: this.state.isAuthorized
            }}>{this.props.children}</AuthContext.Provider>
        )
    }
}

export {AuthProvider, AuthContext}