import React from 'react';
import {Redirect, Switch} from "react-router-dom";
import AuthenticatedRoute from "./AuthenticatedRoute";
import UnauthenticatedRoute from "./UnauthenticatedRoute";
import Menu from "../components/Menu";
import Login from "../components/auth/Login";
import Profile from "../components/Profile";
import Registration from "../components/auth/Registration";
import PasswordRestore from "../components/auth/PasswordRestore";
import TrainSchedule from "../components/pages/TrainSchedule";
import TrainRoute from "../components/pages/TrainRoute";
import RouteEdge from "../components/pages/RouteEdge";
import WayPointEdit from "../components/pages/WayPointEdit";
import NewRide from "../components/pages/NewRide";
import Trains from "../components/pages/Trains";

/**
 * Глобальный свитч путей
 */
export default class RouteSwitch extends React.Component {
        render() {
                return (
                    <Switch>
                            <AuthenticatedRoute
                                component={<Menu/>}
                                path="/menu">
                            </AuthenticatedRoute>
                            <AuthenticatedRoute
                                component={<TrainSchedule/>}
                                path="/train-schedule">
                            </AuthenticatedRoute>
                            <AuthenticatedRoute
                                component={<Profile/>}
                                path="/profile">
                            </AuthenticatedRoute>
                            <AuthenticatedRoute
                                component={<TrainRoute/>}
                                path="/train-route/:rideId">
                            </AuthenticatedRoute>
                            <AuthenticatedRoute
                                component={<RouteEdge/>}
                                path="/route-edge/:rideId">
                            </AuthenticatedRoute>
                            <AuthenticatedRoute
                                component={<WayPointEdit/>}
                                path="/waypoint-edit/:rideId">
                            </AuthenticatedRoute>
                            <AuthenticatedRoute
                                component={<Trains/>}
                                path="/trains">
                            </AuthenticatedRoute>
                            <AuthenticatedRoute
                                component={<NewRide/>}
                                path="/new-ride/:trainId">
                            </AuthenticatedRoute>
                            <UnauthenticatedRoute
                                component={<Login/>}
                                path="/login">
                            </UnauthenticatedRoute>
                            <UnauthenticatedRoute
                                component={<Registration/>}
                                path="/registration">
                            </UnauthenticatedRoute>
                            <UnauthenticatedRoute
                                component={<PasswordRestore/>}
                                path="/restore">
                            </UnauthenticatedRoute>
                            <Redirect from='/' to='/menu'/>
                    </Switch>
                );
        }
}
